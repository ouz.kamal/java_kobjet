package _07_Interfaces.Task2;

import java.util.List;

public interface ProductRepository {
	
	/*
	 * C.R.U.D.
	 * 
	 * C : Create
	 * R : Read
	 * U : Update
	 * D : Delete
	 */
	
	List<Product> getAll();
	
	void addProduct(Product p);
	
	void updateProduct(Product p);
	
	void deleteProduct(int id);
}
