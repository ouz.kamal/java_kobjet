package _01_Constructeur;

public class User {

	// Variables d'instance : chaque instance de la classe User a son propre nom, prenom et age
	public String nom;
	public String prenom;
	public int age;
	
	// Le mot cl� "static" permet de d�finir des varables de Classe (par opposition aux varaibles d'instances)
	// nUsers est une variable de Classe car elle est commune � toutes les instances
	public static int nUsers;
	
	/*
	 * Un constructeur est une m�thode sp�ciale :
	 * - qui porte le m�me nom que la classe
	 * - qui ne retourne rien
	 * - qui sert � instancier (� cr�er des instances de la classe)
	 */
	
	// Constructeur sans param�tre
	public User()
	{
		System.out.println("Appel du constructeur sans param�tre de la classe User");
		nUsers++;
	}
	
	// Constructeur � 2 param�tres (de type String)
	public User(String nom, String prenom)
	{
		// "this" fait r�f�rence � l'objet courant
		this(); // Appel au constructeur sans param�tre
		this.nom = nom;
		this.prenom = prenom;
		//nUsers++;
		
		System.out.println("Appel du constructeur � 2 param�tres de la classe User");
	}

	public User(String nom, String prenom, int age) {
		
		//super(); // Appel au constructeur de la "super classe" (ou classe m�re).
		
		/*
		 * En java toute les classes h�ritent implicitment de la classe "Object"
		 * Comme notre classe "User" n'h�rite pas d'autre classe, sa super classe est donc la classe "Object"
		 */
		
		this(nom, prenom); // Appel au constructeur de la classe User � 2 param�tre de type Stringg nom et premom
//		this.nom = nom; // Inutile car redondant avec le co,nstructeur � 2 param�tres
//		this.prenom = prenom; // Idem...
		this.age = age;
		
		System.out.println("Appel du constructeur � 3 param�tres de la classe User");
		
		//nUsers++;
	}

	@Override
	public String toString() {
		return prenom + " " + nom + " a " + age + " ans.";
	}
	
	
}
