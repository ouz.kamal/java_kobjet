package _03_Agregation;

public class Notes {

	int maths; // private par d�faut...
	
	int english;
	
	public Notes(int maths, int english) {
		super();
		this.maths = maths;
		this.english = english;
	}

	@Override
	public String toString() {
		return "Notes [maths=" + maths + ", english=" + english + "]";
	}
}
