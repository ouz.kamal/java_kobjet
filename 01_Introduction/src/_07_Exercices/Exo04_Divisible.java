package _07_Exercices;

import java.util.Scanner;

public class Exo04_Divisible {

	/*
	 * V�rifier si un nombre est divisible par 3 et 13 ou non.
	 * Un nombre entier est divisible par un autre quand le r�sultat de la division euclidienne est un entier sans reste.
	 * On peut �crire b = k*a + R, avec R = 0.
	 */
	
	public static void main(String[] args) {
		
		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Entrez un nombre :");
		
		int n = clavier.nextInt();
		
		if(n % 3 == 0 && n % 13 == 0 )
		{
			System.out.println(n + " est divisible par 3 et 13");
		}
		else
		{
			System.out.println(n + " n'est divisible par 3 et 13");
		}
		
		clavier.close();	
	}
}
