package _07_Exercices;

import java.util.Scanner;

public class Exo05_Bissexstile {

	/*
	 * V�rifier si l'ann�e donn�e par l'utilisateur est bissextile (366 jours) ou non.
	 * Une ann�e est consid�r�e comme une ann�e bissextile si :
	 * elle est divisible par 4 et non divisible par 100 ;
	 *  ou si elle est divisible par 400
	 * � divisible � signifie que la division donne un nombre entier, sans reste (c'est � dire le reste �gale � z�ro) : 21 est divisible par 3. 22 non.
	 */
	
	public static void main(String[] args) {
		
		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Saisir un ann�e :");
		
		int annee = clavier.nextInt();
		
		if((annee % 4 == 0 && annee % 100 != 0) || annee % 400 == 0)
		{
			System.out.println(annee + " est une ann�e bissexstile");
		}
		else
		{
			System.out.println(annee + " n'est pas une ann�e bissexstile");
		}
		
		clavier.close();
	}
}
