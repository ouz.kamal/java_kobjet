package _05_Instructions;

import java.util.Scanner;

/*
 * Une boucle permet de r�p�ter (it�rr) un certain nombre de fois une instruction ou un bloc d'instructions.
 * 
 * 3 mots cl�s permettent de cr�er des boucles :
 * - for
 * - while
 * - do while
 */
public class _03_Boucles {

	public static void main(String[] args) {
		
		/*
		 * La boucle for permet de r�p�ter un blocs d'instructions un nombre d�temrin�  fois.
		 * 
		 * Syntaxe : for(initExpr; testExpr; incExpr){ Bloc d'instructions � r�p�ter}
		 * 
		 * Avec :
		 * - initExpr : expression d'initialisation
		 * - testExpr : expression de test
		 * - incExpr : expression d'incr�mentation
		 *  
		 */
		
		String[] months = {"Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"};
		
		System.out.println("**************** BOUCLE FOR ********************");
		for(int index = 0; index < months.length; index++)
		{
			System.out.println(months[index]);
		}
		
		System.out.println("\n**************** BOUCLE FOR AVEC BREAK ********************");
		for(int index = 0; index < months.length; index++)
		{
			if (months[index] == "Octobre") {
				System.out.println("L'instruction 'break' permet de stopper, sous condition, l'ex�cution d'une bouble");
				break;
			}
			System.out.println(months[index]);
		}
		
		System.out.println("\n**************** BOUCLE FOR AVEC CONTINUE ********************");
		for(int index = 0; index < months.length; index++)
		{
			if (months[index] == "Octobre") {
				System.out.println("L'instruction 'continue' permet de passer directement � l'it�ration suivante");
				continue;
			}
			System.out.println(months[index]);
		}
		
		/*
		 * La boucle while permet de r�p�ter un bloc d'instructions tant qu'une condition est v�rifi�e.
		 * 
		 * Syntaxe : while(testExpr){Bloc d'instructions � it�rer}
		 * 
		 * Avec : testExpr : expression de test
		 */
		
		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Entrez votre age : ");
		
		int age = clavier.nextInt();
		
		while(age <= 0)
		{
			System.out.println("Entrez un age positif : ");
			
			age = clavier.nextInt();
		}
		
		System.out.println("Vous avez " + age + " ans.");
		
		/*
		 * La boucle Do While se diff�rencie de la boucle While en ce que la condition est v�rifi�e apr�s l'ex�cution du bloc d'instructions.
		 * On est donc certain d'ex�cuter au moins une fois le bloc d'instructions
		 */
		
		age = 0;
		
		do {
			System.out.println("Entrer l'age de votre chien :");
			
			age = clavier.nextInt();
		}
		while(age <= 0);
		
		System.out.println("Votre chien a " + age + " ans.");
		
		clavier.close();
	}
}
