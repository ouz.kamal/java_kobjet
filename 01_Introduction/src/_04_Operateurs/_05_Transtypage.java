package _04_Operateurs;

public class _05_Transtypage {

	/*
	 * TRANSTYPAGE : transformer un type en un autre type
	 */
	public static void main(String[] args) {
		
		/*
		 * TRANSTYPAGE IMPLICITE : On convertit un type plus petit (en m�moire) vers
		 * un type plus grand, ou un entier vers un nimbre � virgule flottante.
		 * => pas de rique de perte de pr�cision
		 */
		
		byte myByte = 111;
		
		short myShort = myByte;
		
		int myInt = myShort;
		
		long myLong = myInt;
		
		float myFloat = myLong;
		
		double myDouble = myFloat;
		
		/*
		 * TRANSTYPAGE EXPLICITE : On convertit un type plus grand (en m�moire) vers un type plus petit,
		 * ou un nombre � virgule flottante vers un entier
		 * => Risque de perte de pr�cision
		 * => cast obligatoire
		 */
		myDouble = 12345678901.123456789;
		
		//myFloat = myDouble; // Type mismatch: cannot convert from double to float
		
		myFloat = (float) myDouble; // "cast" d'une variable de type double en variable de type float
		myLong = (long) myFloat;
		myInt = (int) myLong;
		myShort = (short) myInt;
		myByte = (byte) myShort;
		
		System.out.println(myDouble + " (myDouble)");
		System.out.println(myFloat + " (myFloat)");
		System.out.println(myLong + " (myLong)");
		System.out.println(myInt + " (myInt)");
		System.out.println(myShort + " (myShort)");
		System.out.println(myByte + " (myByte)");
		
		// ATTENTION !!! Il faut que le type qui re�oit ait la capacit� de contenir la valeur 
		
		int i = 130;
		byte b = (byte) i;
		
		System.out.println(b); // !!!!!-126 car byte compris entre -128 et 127. Donc 130 est trop grand!
		
		// int vs byte
		// 127 vs 127
		// 128 vs -128
		// 129 vs -127
		// 130 vs -126
	}
}
